/* o65 loader */
/* TODO: optimise by precalculating values in get_reloc */
/* TODO: cleanup */

.include "o65.inc"
.include "ioapi.inc"
.include "mem.inc"

/* FIXME: shouldn't need to use either */
.import get_cpid
.include "tty.inc"

/*  IN: A low fname  X high fname  Y start of stack space
 * OUT: A: 0 = success
 *         1 = file doesn't exist
 *         2 = error during parse
 *     X: page of program start
 *     Y: size of program in pages
 * NOT REENTRANT : INTERRUPTS HAVE TO BE OFF
 */
.export load_o65
load_o65:
	STA __IOBASE__+$80
	STX __IOBASE__+$81
	STY reloc_zp_start

	STZ mem_addr
	STZ mem_addr + 1
	STZ mem_size

	STZ buffer_i + 1
	LDA #$FF
	STA buffer_i

	LDY #$00
@loop:
	JSR read_byte
	STA header, Y
	INY
	CPY #26
	BNE @loop

	/* ensure the magic matches */
	LDA header
	CMP #O65_MARKER_0
	BNE @magic_fail
	LDA header + 1
	CMP #O65_MARKER_1
	BNE @magic_fail
	LDA header + 2
	CMP #O65_MAGIC_0
	BNE @magic_fail
	LDA header + 3
	CMP #O65_MAGIC_1
	BNE @magic_fail
	LDA header + 4
	CMP #O65_MAGIC_2
	BNE @magic_fail

	BRA :+
@magic_fail:
	LDA #<@magic_fail_msg
	LDX #>@magic_fail_msg
	JMP fail
@magic_fail_msg:
	.byte "[o65] not a o65 file", $0A, $00

:

	/* skip all headers */
@loop_opts:
	JSR read_byte
	BEQ @loop_opts_done

	STA tmp1

	CMP #2 /* min length 2 */
	BCC @loop_opts_fail
@loop_opts_skip:
	DEC tmp1
	BEQ @loop_opts
	JSR read_byte
	BRA @loop_opts_skip
@loop_opts_fail:
	LDA #<@loop_opts_fail_msg
	LDX #>@loop_opts_fail_msg
	JMP fail
@loop_opts_fail_msg:
	.byte "[o65] loop opts failure!", $0A, $00
@loop_opts_done:
	LDA header + O65_HDR::ZLEN
	CMP #32 /* FIXME: hardcoded */
	BCC :+
	/* zero page size of program too big */
	LDA #<@msg_zp_fail
	LDX #>@msg_zp_fail
	JMP fail
@msg_zp_fail:
	.byte "[o65] zeropage too big", $0A, $00
:
	LDA header + O65_HDR::STACK
	CMP #32
	BCC :+
	/* stack size of program too big */
	LDA #<@msg_stack_fail
	LDX #>@msg_stack_fail
	JMP fail
@msg_stack_fail:
	.byte "[o65] stack too big", $0A, $00
:

	/* calculate memory needed to be allocated */
	CLC
	LDA header + O65_HDR::TLEN
	ADC header + O65_HDR::DLEN
	STA load_len
	LDA header + O65_HDR::TLEN + 1
	ADC header + O65_HDR::DLEN + 1
	BCS @fail_overflow /* overflow failure */
	STA load_len + 1

	LDA load_len
	CLC
	ADC header + O65_HDR::BLEN
	TAX

	LDA load_len + 1
	ADC header + O65_HDR::BLEN + 1
	BCS @fail_overflow /* overflow failure */
	TAY

	BRA :+
@fail_overflow:
	LDA #<@fail_overflow_msg
	LDX #>@fail_overflow_msg
	JMP fail
@fail_overflow_msg:
	.byte "[o65] overflow fail", $0A, $00
:
	/* tlen+dlen+blen in Y(high),X(low) */

	/* round up to pages */
	CPX #$00
	BEQ :+
	INY
:
	STY mem_size

	/* allocate memory */
	/* TODO: use syscall */
	/* TODO: don't assume pid */
	JSR get_cpid
	; assume pid between 0000.0000 to 0000.0111
	ASL ; 0000.___0
	ASL ; 000_.__00
	ASL ; 00__._000
	ASL ; 0___.0000
	ORA #$0F ; 0___.1111
	JSR mem_alloc_pages
	CPX #$00
	BNE :+
	LDA #>@oom_msg
	LDX #<@oom_msg
	JMP fail

@oom_msg:
	.byte "[o65] out of memory", $0A, $00

:
	STX mem_addr + 1
	STZ mem_addr

;	/* print page address of allocated memory */
;	STX HOUT
;	LDA #$0A
;	STA COUT

	/* load text and data segment from file */
	LDA load_len
	STA zp_tmp0
	LDA load_len + 1
	STA zp_tmp0 + 1
	LDA mem_addr
	STA zp_tmp1
	LDA mem_addr + 1
	STA zp_tmp1 + 1
	LDY #$00
@load_loop:
	LDX zp_tmp0 + 1
	BEQ :++
:
	JSR read_byte
	STA (zp_tmp1),Y
	INY
	BNE :-
	INC zp_tmp1 + 1
	DEX
	BNE :-

:	LDX zp_tmp0
	BEQ @load_done
:
	JSR read_byte
	STA (zp_tmp1),Y
	INY
	DEX
	BNE :-
@load_done:

	/* TODO: handle undefined refrences */
	/* TODO: handle undefined __JSRTBL__ */
	JSR read_byte
	BNE @undefined_ref_fail
	JSR read_byte
	BNE @undefined_ref_fail
	BRA :+
@undefined_ref_fail:
	LDA #<@undefined_ref_fail_msg
	LDX #>@undefined_ref_fail_msg
	BRA @fail
@undefined_ref_fail_msg:
	.byte "[o65] undefined ref!", $0A, $00
:

	/* reloc text */
	LDA mem_addr
	LDX mem_addr + 1
	JSR reloc_seg

	/* reloc data */
	LDA mem_addr
	CLC
	ADC header + O65_HDR::TLEN
	PHA
	LDA mem_addr + 1
	ADC header + O65_HDR::TLEN + 1
	TAX
	PLA
	JSR reloc_seg

	/* TODO: handle exports */
	/* TODO: support _start entry point */

/* OUT: A: 0 = success
           1 = file doesn't exist
           2 = error during parse
        X: page of program start
        Y: size of program in pages
*/
	LDA #$00
	LDX mem_addr + 1
	LDY mem_size
	RTS

@fail:
	/* FIXME: this gets called with elements still on the stack **facepalm** */
	LDX mem_addr + 1
	LDY mem_size
	JSR mem_free_pages
	LDA #<@fail_msg
	LDX #>@fail_msg
	JMP fail
@fail_msg:
	.byte "[o65] load fail!", $0A, $00
/* print message in A/X return error */
fail:
	/* free memory pages if we allocated any */
	PHX
	LDX mem_addr + 1
	BEQ :+
	LDY mem_size
	JSR mem_free_pages
:
	PLX

	JSR kputs
	LDA $FF
	RTS

reloc_seg:
	; start value = segment - 1
	SEC
	SBC #1
	BCS :+
	DEX
:
	; reloc data in zp_tmp0
	STA zp_tmp0
	STX zp_tmp0 + 1
@loop:
	JSR read_byte
	BEQ @done
	CMP #255
	BNE @normal
	; increment offset by 254
	LDA zp_tmp0
	CLC
	ADC #254
	STA zp_tmp0
	BCC @loop
	INC zp_tmp0 + 1
	BRA @loop
@normal:
	; increment offset by A
	CLC
	ADC zp_tmp0
	STA zp_tmp0
	BCC :+
	INC zp_tmp0 + 1
:
	; read reloc byte, extract segment id, fetch reloc value, place into zp_tmp1
	JSR read_byte
	STA tmp1
	AND #O65_SEGID_MASK
	JSR get_reloc
	STA zp_tmp1
	STX zp_tmp1 + 1

	LDA tmp1
	AND #O65_RTYPE_MASK
	CMP #O65_RTYPE_WORD
	BEQ @reloc_word
	CMP #O65_RTYPE_HIGH
	BEQ @reloc_high
	CMP #O65_RTYPE_LOW
	BEQ @reloc_low
	JMP @fail

@reloc_low:
	LDY #0
	CLC
	LDA zp_tmp1
	BCC @add_common

@reloc_high:
	JSR read_byte
	LDY #0
	CLC
	ADC zp_tmp1
@add_high:
	LDA zp_tmp1 + 1
@add_common:
	ADC (zp_tmp0),Y
	STA (zp_tmp0),Y
	BRA @loop
@done:
	RTS
@fail:
	LDA #<@fail_msg
	LDX #>@fail_msg
	JMP fail
@fail_msg:
	.byte "[o65] reloc fail!", $0A, $00

@reloc_word:
	LDY #0
	CLC
	LDA zp_tmp1
	ADC (zp_tmp0),Y
	STA (zp_tmp0),Y
	INY
	BRA @add_high

/* TODO: subtract segment base address for true relocation, currently everything has to be linked with base 0 */
get_reloc:
	CMP #O65_SEGID_UNDEF
	BEQ @fail
	CMP #O65_SEGID_ABS
	BEQ @fail
	CMP #O65_SEGID_TEXT
	BEQ @text
	CMP #O65_SEGID_DATA
	BEQ @data
	CMP #O65_SEGID_BSS
	BEQ @bss
	CMP #O65_SEGID_ZP
	BEQ @zp
@fail:
	; pull return addr
	PLA
	PLA

	; pull return addr of reloc_seg
	PLA
	PLA

	LDA #>@fail_msg
	LDX #<@fail_msg
	JMP fail

@text:
	LDA #$00
	LDX #$00
	BRA @common_add
@data:
	LDA header + O65_HDR::TLEN
	LDX header + O65_HDR::TLEN + 1

	BRA @common_add
@bss:
	LDA header + O65_HDR::TLEN
	CLC
	ADC header + O65_HDR::DLEN
	PHA
	LDA header + O65_HDR::TLEN + 1
	ADC header + O65_HDR::DLEN + 1
	TAX
	PLA
	BRA @common_add

@zp:
	LDX #$00
	LDA reloc_zp_start
	RTS

@common_add:
	CLC
	ADC mem_addr
	PHA
	TXA
	ADC mem_addr + 1
	TAX
	PLA
	RTS
@fail_msg:
	.byte "[o65] reloc fail", $0A, $00


read_byte:
	PHX
	PHY

	LDX buffer_i
	INX
	STX buffer_i
	BNE @done

	INC buffer_i + 1
	BEQ @fail /* fail */

	LDY buffer_i + 1
	STY __IOBASE__+$83
	LDY #>buffer
	STY __IOBASE__+$84
	LDA #$01
	STA __IOBASE__+$82
	LDA __IOBASE__+$82
	BNE @fail

@done:
	LDA buffer, X
	PLY
	PLX
	CMP #$00
	RTS
@fail:
	PLY
	PLX
	; pull return address
	PLA
	PLA
	LDA #<@msg
	LDX #>@msg
	JMP fail
@msg:
	.byte "[o65] read_byte failure", $0A, $00

.bss
header:
	.tag O65_HDR
buffer_i:
	.res 2, $00
tmp1:
	.res 2, $00
reloc_zp_start:
	.res 1, $00
mem_addr:
	.res 2, $00
mem_size:
	.res 1, $00

load_len:
	.res 2, $00

	.align 256
buffer:
	.res 256, $00

.zeropage
.org $10
zp_tmp0:
	.res 2, $00
zp_tmp1:
	.res 2, $00

zp_tmp2:
	.res 2, $00
