/* TODO: move process switching code around */
/* TODO: hook all interrupts (network, keyboard, etc) */
/* TODO: free kernel space when no longer needed (kinit, etc ...) */
/* main kernel code */

.include "ioapi.inc"
.include "tty.inc"
.include "mem.inc"
.include "halt.inc"

.import netdrv_init
.import fileio_init
.import shell_init
.import shell_start
.import syscall_sysmon
.import process_init

.code
.export kmain
kmain:
	LDA $FE
	STA syscall_sysmon + 1
	LDA $FF
	STA syscall_sysmon + 2
	LDA #<isr
	STA $FFFE
	LDA #>isr
	STA $FFFF

	LDA #<msg_boot
	LDX #>msg_boot
	JSR kputs

	JSR mem_init
	JSR fileio_init
	JSR netdrv_init
	JSR shell_init
	JSR process_init

	LDA #<free_msg1
	LDX #>free_msg1
	JSR kputs

	JSR mem_count_free_blocks
	TYA
	LSR
	LSR
	STA NOUT

	LDA #<free_msg2
	LDX #>free_msg2
	JSR kputs

	JMP shell_start

/* handle IRQ (either BRK or user ctrl+c) */
isr:
	PHA
	PHX
	PHY

	; get the processor flags from the stack
	TSX
	LDA $0104, X
	AND #$10
	BNE :+
	/* user pressed ctrl+c */
	LDA #<msg_irq
	LDX #>msg_irq
	JSR kputs

	JMP syscall_sysmon
:
	JMP process_switch_nosave

/* FIXME: move code that requires this back to process.s */
NUMPROCS = 8
.import current_process
.import stack_table

/* switch task saving everything */
.export process_switch

/* switch task saving everything except the processor flags */
.export process_switch_irq

/* don't save any registers except the stack pointer */
.export process_switch_nosave

/* don't even save the stack pointer this does not return */
.export process_switch_nosave_direct
process_switch:
	PHP
process_switch_irq:
	/* save the stack and registers but no processor flags */
	PHA
	PHX
	PHY
process_switch_nosave:
	/* save the stack pointer, but no registers */
	SEI
	TSX
	TXA
	LDX current_process
	STA stack_table, X
process_switch_nosave_direct:
	/* save absolutely nothing */
	SEI
	CLD
	/* find the next process */
@loop:
	INX
	CPX #NUMPROCS + 1
	BNE @loop_l1
	LDX #$0
@loop_l1:
	CPX current_process
	BNE @found
@test_fail:
	LDA stack_table, X
	BEQ @fail
@found:
	LDA stack_table, X
	CMP #0
	BEQ @loop

	/* switch */
	STX current_process
	LDA stack_table, X
	TAX
	TXS

	PLY
	PLX
	PLA
	RTI
@fail:
	LDA #<@kill_init
	LDX #>@kill_init
	JMP panic
@kill_init:
	.byte "NO PROCESSES LEFT!!", $00

.rodata
msg_boot:
	.byte "********************************", $0A
	.byte "* bOS kernel v0.0.1-git        *", $0A
	.byte "********************************", $0A
	.byte "bOS  Copyright (C) 2018  bauen1 ", $0A
	.byte "This program comes with         ", $0A
	.byte "ABSOLUTELY NO WARRANTY;         ", $0A
	.byte "See LICENSE for details         ", $0A
	.byte "********************************", $0A
	.byte "", $0A
	.byte $00
msg_irq:
	.byte "^C", $0A, "IRQ!!", $0A, $00
free_msg1:
	.byte "[kmain] Memory: ", $00
free_msg2:
	.byte "kb free", $0A, $00
