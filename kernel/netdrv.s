.include "ioapi.inc"
.include "tty.inc"

.code
.export netdrv_init
netdrv_init:
	LDA #<msg
	LDX #>msg
	JSR kputs
	JSR netdrv_up
	LDA #<ip_msg
	LDX #>ip_msg
	JSR kputs
	LDY #$00
	JSR netdrv_printip
	LDA #$0A
	STA COUT
	RTS

.export netdrv_printip
netdrv_printip:
	PHA
	PHX
	LDX #'.'
@loop:
	LDA $E000,Y
	STA __IOBASE__+$D1
	CPY #$03
	BEQ @done
	STX COUT
	INY
	BNE @loop
@done:
	PLX
	PLA
	RTS

.export netdrv_up
netdrv_up:
	LDA #$E0
	STA __IOBASE__+$70 /* network segment */
	RTS

.export netdrv_down
netdrv_down:
	LDA #$00
	STA __IOBASE__+$70 /* network segment */
	RTS

.rodata
msg:
	.byte "net driver v0.1", $0A, $00
ip_msg:
	.byte "IP: ", $00
