.include "ioapi.inc"
.include "tty.inc"
.include "halt.inc"

.code
.export fileio_init
fileio_init:
	LDA #<@msg
	LDX #>@msg
	JSR kputs
	RTS
@msg:
	.byte "[fileio] driver v0.0.1", $0A, $00

.export fileio_fload
fileio_fload:
	STA __IOBASE__+$80
	STX __IOBASE__+$81
	STZ __IOBASE__+$83
	STY __IOBASE__+$84
	LDA #$01
	STA __IOBASE__+$82
	LDA __IOBASE__+$82
	RTS

.export fileio_fsave
fileio_fsave:
	/* TODO: implement */
	LDA #<@msg
	LDX #>@msg
	JMP panic
@msg:
	.byte "fileio_fsave not implemented!", $0A, $00
