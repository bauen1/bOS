/* kinit - low-level init */
.export _kinit
.import kmain

.segment "KINIT"
.org $f000
_kinit:
	JMP kmain
