.include "ioapi.inc"
.include "tty.inc"

/* panic:
 *  panic reason in A/X, 0-terminated string, no newline
 */
.export panic
panic:
	PHA
	PHX
	LDA #<msg_panic
	LDX #>msg_panic
	JSR kputs
	PLX
	PLA
	JSR kputs
	LDA #$0A
	JSR kputc
	/* fall-through */

/* halt the cpu / shutdown */
.export halt
halt:
	LDA #<msg_halting_cpu
	LDX #>msg_halting_cpu
	JSR kputs
	LDA #$78
	STA __IOBASE__+$f2

.rodata
msg_panic:
	.byte "PANIC: ", $00
msg_halting_cpu:
	.byte "Halting the CPU ...", $0A, $00
