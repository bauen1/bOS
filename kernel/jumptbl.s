/* syscall jump table */

.SEGMENT "JUMPTABLE"

.export syscall_putc
.import kputc
syscall_putc:
	JMP kputc

.export syscall_getc
.import kgetc
syscall_getc:
	JMP kgetc

.export syscall_printip
.import netdrv_printip
syscall_printip:
	JMP netdrv_printip

.export syscall_fload
.import fileio_fload
syscall_fload:
	JMP fileio_fload

.export syscall_fsave
.import fileio_fsave
syscall_fsave:
	JMP fileio_fsave

.export syscall_net_up
.import netdrv_up
syscall_net_up:
	JMP netdrv_up

.export syscall_net_down
.import netdrv_down
syscall_net_down:
	JMP netdrv_down

.export syscall_shell_entry
.import shell_entry
syscall_shell_entry:
	JMP shell_entry

.export syscall_getpid
.import get_cpid
syscall_getpid:
	JMP get_cpid

.export syscall_yield
.import process_switch
syscall_yield:
	JMP process_switch

.export syscall_exit
.import process_exit
syscall_exit:
	JMP process_exit

.export syscall_alloc
.import mem_alloc_pages
syscall_alloc:
	JMP mem_alloc_pages

.export syscall_free
.import mem_free_pages
syscall_free:
	JMP mem_free_pages

.export syscall_get_argv
.import get_argv
syscall_get_argv:
	JMP get_argv

.export syscall_halt
.import halt
syscall_halt:
	JMP halt

.export syscall_sysmon
syscall_sysmon:
	; dynamically adjusted by the kernel
	JMP $0000
