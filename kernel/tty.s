/* tty interface */
/* WARNING we are just hoping that two processes don't call kgets or kputs at the same time */
.include "ioapi.inc"

.code
.export kputs

kputs:
	PHP
	PHA
	PHY
	SEI
	STA tmp0
	STX tmp0 + 1
	LDY #$00
@loop:
	LDA (tmp0),Y
	BEQ @done
	STA COUT
	INY
	BNE @loop
@done:
	PLY
	PLA
	PLP
	RTS

.export kgets
kgets:
	PHP
	PHA
	PHX
	PHY
	STA tmp0
	STX tmp0 + 1
	LDY #$00
@loop:
	LDA CIN
	CMP #$0A
	BEQ @done
	STA (tmp0),Y
	INY
	BNE @loop
@done:
	LDA #$00
	STA (tmp0),Y
	PLY
	PLX
	PLA
	PLP
	RTS

.export kputc
kputc:
	STA COUT
	RTS

.export kgetc
kgetc:
	LDA CIN
	RTS

.zeropage
tmp0:
	.res 2
