.import __JSRTBL_START__
__JSRTBL__ = __JSRTBL_START__

syscall_putc     = __JSRTBL__ +  0 * 3
syscall_getc     = __JSRTBL__ +  1 * 3
syscall_printip  = __JSRTBL__ +  2 * 3
syscall_fload    = __JSRTBL__ +  3 * 3
syscall_fsave    = __JSRTBL__ +  4 * 3
syscall_net_up   = __JSRTBL__ +  5 * 3
syscall_net_down = __JSRTBL__ +  6 * 3
syscall_shell    = __JSRTBL__ +  7 * 3
syscall_getpid   = __JSRTBL__ +  8 * 3
syscall_yield    = __JSRTBL__ +  9 * 3
syscall_exit     = __JSRTBL__ + 10 * 3
syscall_alloc    = __JSRTBL__ + 11 * 3
syscall_free     = __JSRTBL__ + 12 * 3
syscall_get_argv = __JSRTBL__ + 13 * 3
syscall_halt     = __JSRTBL__ + 14 * 3
syscall_sysmon   = __JSRTBL__ + 15 * 3
