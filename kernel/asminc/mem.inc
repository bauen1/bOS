.import mem_init
.import mem_free_pages
.import mem_set_pages
.import mem_alloc_pages
.import mem_find_block
.import mem_count_free_blocks
.import page_table
