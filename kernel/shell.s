/* TODO: handle argv on a per-process basis and don't use hardcoded addresses */
/* TODO: per process argv */

.include "ioapi.inc"
.include "tty.inc"

.import run_proc
.import process_switch
.import stack_table
.import syscall_sysmon
/* TODO: replace with hostname system */
.import netdrv_printip

.code

.export shell_init
shell_init:
	RTS

.export shell_start
shell_start:
	LDA #<msg
	LDX #>msg
	JSR kputs
	/* fall-through */

.export shell_entry
shell_entry:
	LDA __IOBASE__+$70
	BEQ @skip_ip
	LDY #$00
	JSR netdrv_printip
@skip_ip:
	LDA #<msg_prompt
	LDX #>msg_prompt
	JSR kputs

	LDA #<buffer
	LDX #>buffer
	JSR kgets

	JSR parse
	STY __argc

	LDA #<argv
	LDX #>argv

	LDA #<buffer
	LDX #>buffer
	JSR run_proc

	CMP #$FF
	BEQ @syntax_err

	TAX
@loop:
	; wait for process to exit
	JSR process_switch
	SEI
	LDA stack_table, X
	BNE @loop
	LDA #<@msg
	LDX #>@msg
	JSR kputs
	CLI
	BRA shell_entry
@msg:
	.byte "[bsh] proc exit", $0A, $00

@syntax_err:
	LDA #<msg_syntax_err
	LDX #>msg_syntax_err
	JSR kputs
	BRA shell_entry

parse_store_argv_ptr:
	PHP
	PHA
	PHX
	PHY
	; buffer, X = start_of_arg
	; Y = num of arg
	TYA
	ASL A
	TAY

	; Y = index of argv
	TXA
	; hope that X didn't overflow
	CLC
	ADC #<buffer
	STA argv, Y
	LDA #>buffer
	BCC @next
	INC A
@next:
	INY
	STA argv, Y

	PLY
	PLX
	PLA
	PLP
	RTS

; Y = number of entries
parse:
	LDX #$00
	LDY #$00
	JSR parse_store_argv_ptr
	LDY #$01
@loop:
	LDA buffer,X
	BEQ @done
	CMP #' '
	BNE @cont
	STZ buffer,X

	INX
	JSR parse_store_argv_ptr
	DEX

	INY
@cont:
	INX
	BNE @loop
@done:
	STZ buffer+1,X
	RTS

.export get_argv
get_argv:
	LDA #<argv
	LDX #>argv
	LDY __argc
	RTS

.rodata
msg:
	.byte "[bsh] bsh v0.1", $0A, $00

msg_syntax_err:
	.byte "[bsh] cmd not found!", $0A, $00

msg_prompt:
	.byte "# ", $00

.bss
.export buffer
buffer:
	.res $100
__argc:
	.res 1
.export argv
argv:
	; FIXME: maybe we need more
	.res $20
