/* Processing subsystem:
 * maximum of 8 tasks
 * each task gets the following memory ranges:
 * stack: (pid<<5) to ((pid + 1)<<5)
 * zeropage: (pid<<5) to ((pid + 1)<<5)
 *
 * the kernel task is pid 7
 * process id's start at 0
 */

.include "ioapi.inc"
.include "tty.inc"
.include "mem.inc"
.import load_o65
.import process_switch_nosave
.import process_switch_nosave_direct

NUMPROCS = 8

.export process_init
process_init:
	LDA #$00
	LDX #8
@loop:
	STA stack_table - 1, X
	DEX
	BNE @loop

	/* pid 7 = kernel task */
	LDA #7
	STA current_process
	RTS

.bss
fname:
	.res 2
parent_pid:
	.res 1
new_stack_top:
	.res 1
new_zero_page:
	.res 1
new_pid:
	.res 1
start_of_program:
	.res 1

.code
/* OUT: A = pid / $FF on error */
/* CALLER IS RESPONSIBLE FOR DISABLING INTERRUPTS */
find_free_pid:
	LDX #$00
@loop:
	LDA stack_table, X
	BEQ @done
	INX
	CPX #NUMPROCS
	BNE @loop
	LDA #$FF
@done:
	RTS

/*  IN: A low fname X high fname */
/* OUT: A = pid / $FF on error  */
.export run_proc
run_proc:
	PHP
	/* a interrupt would be fatal, so disable them */
	SEI

	PHA /* low fname */
	STA fname
	PHX /* high fname */
	STX fname + 1
	PHY

	/* save the current stack in the stack table */
	TSX
	TXA /* A = current_stack */
	LDX current_process
	STA stack_table, X

	STX parent_pid

	/* find a free pid */
	/* FIXME: return different error if no process slots are available */
	JSR find_free_pid
	CMP #$FF
	BEQ @fail
	STX new_pid
	STX current_process /* FIXME: do this later to reduce the code needed */

	/* calculate top of stack */
	TXA
	INC
	ASL ;*2
	ASL ;*4
	ASL ;*8
	ASL ;*16
	ASL ;*32
	STA new_stack_top

	/* calculate start of zero page */
	TXA
	ASL /*  *2 */
	ASL /*  *4 */
	ASL /*  *8 */
	ASL /* *16 */
	ASL /* *32 */
	STA new_zero_page

	/* load the programm from disk */
	LDA fname
	LDX fname + 1
	LDY new_zero_page
	JSR load_o65
	CMP #$00
	BNE @fail

	STX start_of_program /* page of program start */

	/* setup stack frame for new process */
	LDX new_stack_top
	TXS
	LDA start_of_program
	PHA
	LDA #$00
	PHA

	/* processor flags (interrupts enabled) */
	LDA #$0
	PHA

	PHA
	PHX
	PHY
	TSX
	TXA
	LDX current_process
	STA stack_table, X

	/* return to the parent */
	LDX parent_pid
	STX current_process

	LDA stack_table, X
	TAX
	TXS

	/* inform the user of the task start (optional, can be disabled) */
	LDA #<@start_msg
	LDX #>@start_msg
	JSR kputs
	LDA new_pid
	STA NOUT
	LDA #$0A
	STA COUT

	PLY
	PLX
	PLA
	LDA new_pid
	PLP
	RTS
@fail:
	/* inform the user of the failed task start (optional, can be disabled) */
	LDA #<@fail_msg
	LDX #>@fail_msg
	JSR kputs

	LDA parent_pid
	STA current_process

	PLY
	PLX
	PLA
	LDA #$FF
	PLP
	RTS
@fail_msg:
	.byte "[process] failed to run programm!", $0A, $00
@start_msg:
	.byte "[process] start pid: ", $00

/* terminate current process */
.import page_table
.export process_exit
process_exit:
	SEI
	JSR get_cpid

	/* print exit message */
	PHA
	LDA #<@exit_msg
	LDX #>@exit_msg
	JSR kputs
	PLA
	STA NOUT
	LDA #$0A
	STA COUT

	JSR get_cpid

	/* remove the process from scheduling */
	TAX
	STZ stack_table, X

	/* free the memory used */
	ASL
	ASL
	ASL
	ASL
	ORA #$0F
	CLI
	STA tmp0
	LDX #$FF
@loop:
	LDA page_table, X
	CMP tmp0
	BNE @next
	STZ page_table, X
@next:
	DEX
	BNE @loop

	/* todo: free memory looking like: 0___.xxxx ___ = pid xxxx = ?*/
	JMP process_switch_nosave_direct
@exit_msg:
	.byte "[process] exit pid: ", $00

/* returns the pid of the current active process */
.export get_cpid
get_cpid:
	LDA current_process
	RTS

.data
.export current_process
current_process:
	.res 1
.export stack_table
stack_table:
	.res NUMPROCS, 0
tmp0:
	.byte $00
