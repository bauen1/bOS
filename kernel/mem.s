/* FIXME: include asserts for when we do stupid stuff */
.import __BSS_RUN__
.import __BSS_SIZE__

.import __JUMPTABLE_RUN__
.import __JUMPTABLE_SIZE__

.import __KERNEL_START__
.import __KERNEL_SIZE__

.import __ROM_START__
.import __ROM_SIZE__

/*
;.import __KINIT_RUN__
;.import __KINIT_SIZE__

.import __CODE_RUN__
.import __CODE_SIZE__

.import __RODATA_RUN__
.import __RODATA_SIZE__

.import __MAIN_START__
.import __MAIN_SIZE__
*/

/* KERNEL_DEBUG = 1 */

.code
.export mem_init
mem_init:
	LDX #$00
	LDY #$00
	LDA #$00
	JSR mem_set_pages

	LDA #$FF
	STA page_table + $00
	STA page_table + $01
	STA page_table + $FF

	LDA #$ee
	STA page_table + $02 ; buffer used by rom

	/*
	LDA #$F1
	LDX #>(__CODE_RUN__)
	LDY #>(__CODE_SIZE__)
	JSR mem_set_pages

	LDA #$F2
	LDX #>(__RODATA_RUN__)
	LDY #>(__RODATA_SIZE__)
	JSR mem_set_pages

	LDA #$F3
	LDX #>(__BSS_RUN__)
	LDY #>(__BSS_SIZE__)
	JSR mem_set_pages

	LDA #$FF
	LDX #>(__JUMPTABLE_RUN__)
	LDY #>(__JUMPTABLE_SIZE__)
	JSR mem_set_pages
	*/

	LDA #$F0
	LDX #>(__BSS_RUN__)
	LDY #>(__BSS_SIZE__)
	JSR mem_set_pages

	LDA #$F1
	LDX #>(__KERNEL_START__)
	LDY #>(__KERNEL_SIZE__)
	JSR mem_set_pages

	LDA #$FE
	LDX #>(__ROM_START__)
	LDY #>(__ROM_SIZE__)
	JSR mem_set_pages

	/* FIXME: remove this once we removed all hardcoded values  */
	LDA #$FA
	LDX #$F0
	LDY #$10
	JSR mem_set_pages

	LDA #$F2
	STA page_table + >__JUMPTABLE_RUN__
	RTS

/*  IN:  X = start  Y = size */
/* OUT:  Flags modified */
.export mem_free_pages
mem_free_pages:
.IFDEF KERNEL_DEBUG
	PHA
	LDA #'F'
	STA COUT
	STX HOUT
	LDA #' '
	STA COUT
	STY HOUT
	LDA #$0A
	STA COUT
	PLA
.ENDIF
	PHA
	LDA #$00
	JSR mem_set_pages
	PLA
	RTS

/*  IN:  A = value  X = start  Y = size */
/* OUT:  Flags modified */
.export mem_set_pages
mem_set_pages:
	PHX
	PHY
@loop:
	STA page_table, X
	INX
	DEY
	BNE @loop
	PLY
	PLX
	RTS

/* don't call this with A=0 */
/*  IN:  A = value  X = ????  Y = size */
/* OUT:  X = start / 0 on error  Y = ???? Flags modified*/
.export mem_alloc_pages
mem_alloc_pages:
.IFDEF KERNEL_DEBUG
	PHX
	LDX #'A'
	STX COUT
	STA HOUT
	LDX #' '
	STX COUT
	STY HOUT
	LDX #$0A
	STX COUT
	PLX
.ENDIF
	PHA
	JSR mem_find_blocks
	PLA
	CPX #$00
	BEQ @fail
	PHX
	JSR mem_set_pages
	PLX
	CPX #$00
@fail:
	RTS

/*  IN:  Y = size */
/* OUT:  X = start / 0 on error */
/* NOT REENTRANT (uses tmp1)*/
.export mem_find_blocks
mem_find_blocks:
	PHA
	STY tmp1 /* needed size */
	LDX #$00 /* i */
	LDA #$00 /* start */
	LDY #$00 /* found size */

@loop:
	PHA
	LDA page_table, X
	BEQ @block_free
@block_full:
	PLA
	LDA #$00
	LDY #$00
	BRA @next

@block_free:
	PLA
	BNE @block_free_start_already_set
	TXA
@block_free_start_already_set:
	INY
@next:
	CPY tmp1
	BEQ @done
	INX
	BNE @loop
@fail:
	LDA #$00
@done:
	TAX
	PLA
	RTS

/* OUT:  Y = number of free blocks */
.export mem_count_free_blocks
mem_count_free_blocks:
	PHA
	PHX

	LDX #$00
	LDY #$00
@loop:
	LDA page_table, X
	BNE @next
	INY
@next:
	INX
	BNE @loop

	PLX
	PLA
	RTS

.bss
.export page_table
page_table:
	.align 256 /* better debugging + performance */
	.res 256, $00
@end:

tmp1:
	.res 1, $00
