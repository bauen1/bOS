# bOS v0.1
custom operating system for [hackers-edge](www.hackers-edge.com)

# Features
 * kernel shell
 * memory allocation
 * a few demo programs

# Files

BOOT.SYS    bootloader
KERNEL.SYS  kernel

sysmon      enter the system monitor
clear       clear screen
halt        shutdown
