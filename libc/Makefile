OBJS := $(subst .s,.o,$(wildcard *.s))
DEPS = $(OBJS:.o=.d)

CPU := 65C02
ASFLAGS  := --cpu $(CPU) -g
ASFLAGS  += --feature c_comments
ASFLAGS  += -I $(SYSROOT)/asminc
ASFLAGS  += -I../machine/asminc
CPPFLAGS := -D__STDLIBC__
CFLAGS   := --cpu $(CPU) -O -g
CFLAGS   += -I $(SYSROOT)/include

NONE_LIB ?= $(realpath $(shell cl65 --print-target-path)/../lib/none.lib)

.SUFFIXES:

.PHONY: all
all: libc.lib

libc.lib: $(OBJS)
	cp $(NONE_LIB) $@
	ar65 a $@ $(OBJS)

%.o %.lst: %.s
	ca65 $(CPPFLAGS) $(ASFLAGS) --create-dep $*.d -l $*.lst -o $*.o $<

%.s: %.c
	cc65 $(CPPFLAGS) $(CFLAGS) -o $@ $<

.PHONY: install-headers
install-headers: install-headers-c

.PHONY: install-headers-c
install-headers-c: include
	mkdir -p $(DESTDIR)/include
	cp -r include/* $(DESTDIR)/include

.PHONY: install-libs
install-libs: libc.lib
	mkdir -p $(DESTDIR)/libs
	cp libc.lib $(DESTDIR)/libs

.PHONY: install-cfgs
install-cfgs: cfgs
	mkdir -p $(DESTDIR)
	cp -r cfgs $(DESTDIR)/

.PHONY: install
install: install-headers install-libs install-cfgs all

.PHONY: clean
clean:
	$(RM) -f *.o *.lst *.map *.bin *.dbg *.d *.lib

-include $(DEPS)
