.include "ioapi.inc"

.export _gotoxy, gotoxy
.import popa

gotoxy:
_gotoxy:
	STA __IOBASE__+$D7
	JSR popa
	STA __IOBASE__+$D6
	RTS
