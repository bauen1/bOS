.export __STARTUP__ : absolute = 1

.export _init, _exit

.import zerobss, initlib, donelib
.import callmain

.include "zeropage.inc"
.include "kernel/syscalls.inc"

.segment "STARTUP"
_init:
	CLD
	LDA #<stack_top
	STA sp
	LDA #>stack_top
	STA sp+1

	JSR zerobss
	JSR initlib
	JSR callmain

_exit:	PHA
	JSR donelib
	PLA
	JMP syscall_exit

.bss
.align 256
.res 256, $00
stack_top:
