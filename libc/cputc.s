.include "kernel/syscalls.inc"

.export _cputc
_cputc:
	JMP syscall_putc
