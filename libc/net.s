.include "zeropage.inc"
.include "kernel/syscalls.inc"
/* FIXME: yeah no */
.include "../../machine/asminc/ioapi.inc"
.import popa, popax

.export _net_listen
.proc _net_listen: near
	STA NETIRQ
	STX NETIRQ + 1
	JSR popa
	STA NETPORT
	LDA #NLISTEN
	STA NETCNTL
	LDA NETCNTL
	BNE :+
	LDX NETIDX
	TXA
	RTS
:	LDA #$FF
	RTS
.endproc

.export _net_close
.proc _net_close: near
	STA NETIDX
	LDA #NSTOP
	STA NETCNTL
	LDA NETCNTL
	RTS
.endproc

.export _net_connect
.proc _net_connect: near
	STA NETIRQ
	STX NETIRQ + 1
	JSR popa
	STA NETPORT
	JSR popax
	STA NETIP
	STX NETIP + 1
	LDA #NCONN
	STA NETCNTL
	LDA NETCNTL
	BNE :+
	LDA NETIDX
	RTS
:
	LDA #$FF
	RTS
.endproc

.export _net_write_buffer
.proc _net_write_buffer: near
	STA ptr1
	STX ptr1 + 1
	STY tmp1
	LDY #00
@loop:	LDA (ptr1), Y
	STA NETOUT
	INY
	CPY tmp1
	BNE @loop
	RTS
.endproc

.export _net_send
.proc _net_send: near
/*	extern int net_send(unsigned char fd, unsigned char length, void *data); */
	PHA
	JSR popa
	TAY
	PLA
	JSR _net_write_buffer
	JSR popa
	STA NETIDX
	LDA #NSEND
	STA NETCNTL
	LDA NETCNTL
	BNE :+
	LDA $00
	RTS
:
	LDA $FF
	RTS
.endproc
