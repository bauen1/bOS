.include "zeropage.inc"
.include "kernel/syscalls.inc"

.import __argc, __argv

.segment "ONCE"
.constructor initmainargs, 24
initmainargs:
	JSR syscall_get_argv
	STA __argv
	STX __argv + 1
	STY __argc
	STZ __argc + 1
	RTS
