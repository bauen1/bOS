#ifndef NET_H
#define NET_H 1

extern unsigned char net_listen(unsigned char port, void *callback);
extern unsigned char net_connect(unsigned char *ipaddr, unsigned char port, void *callback);
extern int net_send(unsigned char fd, unsigned char length, const char *data);
extern void net_close(unsigned char fd);

#endif
