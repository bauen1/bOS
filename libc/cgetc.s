.include "kernel/syscalls.inc"

.export _cgetc
_cgetc:
	; TODO: blinking cursor
	; https://github.com/cc65/cc65/blob/master/include/conio.h#L139
	JMP syscall_getc
