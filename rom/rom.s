.include "ioapi.inc"

.import BOOTLOADER_ADDRESS: absolute

.import puts
.import puts_dots
.import sysmon

.segment "STARTUP"
.export _rom_entry
_rom_entry:
	/* init the cpu to sane defaults */
	LDX #$FF
	TXS
	CLD
	JMP main

.code
main:
	LDA #<welcome_msg
	LDX #>welcome_msg
	JSR puts

	/* build the jump table */
	LDA #$4C ; JMP abs
	STA $FE00
	LDA #<puts
	STA $FE01
	LDA #>puts
	STA $FE02
	LDA #<sysmon
	STA $FFFA
	LDA #>sysmon
	STA $FFFB
	/* fall-through */
try_boot:
	STZ __IOBASE__+$8A
	/*STZ __IOBASE__+$83*/
	LDA #>BOOTLOADER_ADDRESS
	STA __IOBASE__+$84
	LDA #<boot_name
	STA __IOBASE__+$80
	LDA #>boot_name
	STA __IOBASE__+$81
	LDA #$01
	STA __IOBASE__+$82
	LDA __IOBASE__+$82
	BNE @fail
	LDA #<booting_msg
	LDX #>booting_msg
	JSR puts_dots

	JMP BOOTLOADER_ADDRESS
@fail:
	LDA #<fail_msg
	LDX #>fail_msg
	JSR puts
	LDA #<boot_name_space
	LDX #>boot_name_space
	JSR puts_dots
	/*BRA sysmon*/
	JMP sysmon

.rodata
welcome_msg:
	.byte "BootROM v0.2", $0A, $00

fail_msg:
	.byte "coulnd't find", $00

booting_msg:
	.byte "loading"
	/* fall-through */
boot_name_space:
	.byte " "
	/* fall-through */
boot_name:
	.byte "BOOT.SYS", $00
