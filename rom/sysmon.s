/* hemon system monitor interface */

.include "ioapi.inc"

.import puts

.code
.export sysmon
sysmon:
	LDA #<sysmon_msg
	LDX #>sysmon_msg
	JSR puts
	LDA #<buffer
	STA __IOBASE__+$f0
	LDA #>buffer
	STA __IOBASE__+$f1
	LDA #$30
	STA __IOBASE__+$f2
@next_line:
	LDA #'*'
	STA COUT
	LDY #$0
@read_loop:
	LDA CIN
	CMP #$0a
	BEQ @run_line
	STA buffer,Y
	INY
	BRA @read_loop
@run_line:
	LDA #$0
	STA buffer,Y
	LDA __IOBASE__+$f2
/*	BNE @syntax*/
	BEQ @next_line
@syntax:
	BPL @jump_to_code
	LDA #<syntax_error
	LDX #>syntax_error
	JSR puts
	BRA @next_line
@jump_to_code:
	JMP (__IOBASE__+$f3)

.bss
buffer: .res 40, $00

.rodata
sysmon_msg:
	.byte "$SYSMON$", $00
syntax_error:
	.byte "!SYNTAX", $0A, $00
