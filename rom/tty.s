.include "ioapi.inc"

.code
.export puts
.export puts_dots
puts_dots:
	JSR puts
	LDA #<dots_msg
	LDX #>dots_msg
	/* fall-through */
puts:
	STA $80
	STX $81
	LDY #$00
@loop:
	LDA ($80),Y
	BEQ @done
	STA COUT
	INY
	BNE @loop
@done:
	RTS

.rodata
dots_msg:
	.byte " ...",$0A,$00
