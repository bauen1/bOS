/* TODO: ROM Mask works similar, bit 5 is set on $ff1a, remove bit to disable ROM access. */
.include "ioapi.inc"

.import BOOTLOADER_ADDRESS: absolute
.import KERNEL_ADDRESS: absolute

PUTS = $FE00
SYSMON = $FEFD

.code
	/* reset the CPU to sane defaults */
	CLI
	CLD
	LDX #$FF
	TXS

	/* get the sysmon entry point and store it at $00FE/$00FF */
	LDA $FFFA
	STA $FE
	LDA $FFFB
	STA $FF

	LDA #<msg
	LDX #>msg
	JSR PUTS

	/* load the kernel from disk */
	STZ __IOBASE__+$83
	LDA #>KERNEL_ADDRESS
	STA __IOBASE__+$84
	LDA #<kernel_name
	STA __IOBASE__+$80
	LDA #>kernel_name
	STA __IOBASE__+$81
	LDA #$01
	STA __IOBASE__+$82
	LDA __IOBASE__+$82
	BNE @fail

	/* TODO: cmdline, something like that ? */

	JMP KERNEL_ADDRESS

@fail:
	LDA #<fail_msg
	LDX #>fail_msg
	JSR PUTS
	LDA __IOBASE__+$E0
	JMP SYSMON

.rodata
kernel_name:
	.byte "KERNEL.SYS",$00

msg:
	.byte "BOOT.SYS loaded.", $0A
	.byte "Loading KERNEL.SYS ...", $0A, $00

fail_msg:
	.byte "Boot Error, cannot load KERNEL.SYS!", $0A, $00
