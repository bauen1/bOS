OBJS := $(subst .s,.o,$(wildcard *.s))
DEPS  = $(OBJS:.o=.d)

CPU := 65C02
BOOTLOADER_ADDRESS := 0x0200
KERNEL_ADDRESS := 0xF000
LDFLAGS := --start-addr $(BOOTLOADER_ADDRESS) -D BOOTLOADER_ADDRESS=$(BOOTLOADER_ADDRESS) -D KERNEL_ADDRESS=$(KERNEL_ADDRESS)
ASFLAGS := --feature c_comments -g --cpu $(CPU) -I../machine/asminc
CPPFLAGS := -D__BOOTLOADER__

.SUFFIXES:

.PHONY: all
all: boot.bin

boot.hex: boot.bin
	../tools/bin2hex.py --format 2 --binaries=$(BOOTLOADER_ADDRESS),boot.bin -o $@

boot.bin boot.dbg boot.map: boot.cfg $(OBJS)
	ld65 $(LDFLAGS) -C boot.cfg -vm -m boot.map --dbgfile boot.dbg -o $@ $(OBJS)

%.o %.lst: %.s
	ca65 $(CPPFLAGS) $(ASFLAGS) -l $*.lst -o $*.o $^

.PHONY: install
install: all
	mkdir -p $(DESTDIR)
	cp boot.bin $(DESTDIR)/boot.bin

.PHONY: clean
clean:
	$(RM) -f *.o *.lst *.map *.bin *.hex *.dbg *.d

-include $(DEPS)
