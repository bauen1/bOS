#!/usr/bin/env expect

set host "ftp.hackers-edge.com"
catch {set host $env(UPLOAD_SERVER)}
set port 2121
catch {set port $env(UPLOAD_PORT)}
set user "bauen3"
catch {set user $env(UPLOAD_USER)}
set vm_ip "96.219.144.201"
catch {set vm_ip $env(UPLOAD_VM)}

stty -echo
send_user -- "password for $user@$host:$port :"
expect_user -re "(.*)\n"
send_user "\n"
stty echo
set pass $expect_out(1,string)

spawn ftp --passive $host $port

expect "Name"
send "$user\n"

expect "Password"
send "$pass\n"

expect {
	"430 Authentication failed." {exit 1;}
	"230 Login successful."
}
expect "ftp>"
send "binary\n"

proc ftp_delete_file {file} {
	global vm_ip
	expect "ftp>"
	send "delete /$vm_ip/$file\n"
}

#ftp_delete_file "BOOT.SYS"
#ftp_delete_file "KERNEL.SYS"
#ftp_delete_file "clear"
#ftp_delete_file "halt"
#ftp_delete_file "helloworld"
#ftp_delete_file "sysmon"

expect "ftp>"
send "put sysroot/boot.bin /$vm_ip/BOOT.SYS\n"
expect "ftp>"
send "put sysroot/kernel.bin /$vm_ip/KERNEL.SYS\n"

proc put_file {file} {
	global vm_ip
	expect "ftp>"
	send "put sysroot/$file.bin /$vm_ip/$file\n"
	expect {
		"552 File too large" {exit 2;}
		"226 Transfer complete."
	}
}

put_file "clear"
put_file "halt"
put_file "helloworld"
put_file "ls"
put_file "sysmon"

expect "ftp>"
send "put README.md /$vm_ip/README\n"

#interact
expect "ftp>"
send "quit"

send_user "\n"
