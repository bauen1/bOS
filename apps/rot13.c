#include <conio.h>

unsigned char rot13b(unsigned char c, unsigned char base) {
	return (((c - base)+13)%26)+base;
}

unsigned char rot13(unsigned char c) {
	if ('a' <= c && c <= 'z') {
		return rot13b(c, 'a');
	} else if ('A' <= c && c <= 'Z') {
		return rot13b(c, 'A');
	} else {
		return c;
	}
}

int main(int argc, char *argv[]) {
	int i = 0;
	unsigned char c;
	(void)argc;
	(void)argv;
	cputs("enter your code (empty line to quit):\n");
	while ((c = cgetc()) != 0) {
		if (c == '\n') {
			cputc('\n'),
			i++;
			if (i == 2) {
				break;
			}
		} else {
			i = 0;
			cputc(rot13(c));
		}
	}
	cputc('\n');
	return 0;
}
