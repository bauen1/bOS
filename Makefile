# disable all default rules
.SUFFIXES:

export SYSROOT:=$(PWD)/sysroot

MAKE:=make

.PHONY: all
all: all-rom all-boot all-kernel sysroot

.PHONY: all-%
all-%:
	$(MAKE) -C $* --no-print-directory all

.PHONY: sysroot-%
sysroot-%: all-%
	$(MAKE) --no-print-directory -C $* DESTDIR="$(SYSROOT)" install

.PHONY: sysroot
sysroot: sysroot-rom sysroot-boot sysroot-kernel sysroot-libc sysroot-apps

.PHONY: clean
clean: clean-rom clean-boot clean-kernel clean-libc clean-apps clean-sysroot

.PHONY: clean-%
clean-%:
	$(MAKE) -C $* --no-print-directory clean

.PHONY: clean-sysroot
clean-sysroot:
	rm -rf $(SYSROOT)

.PHONY: todo
todo:
	grep --color=always "TODO" -r .
	grep --color=always "FIXME" -r .
